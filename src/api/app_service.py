from fastapi import FastAPI
from .data_model import UserRequest, UserResponse
from src.celery.celery_app import apply_model_to_text


app = FastAPI()


@app.post("/ask_model", response_model=UserResponse)
async def ask_model(req: UserRequest):
    task = apply_model_to_text.delay(req.model_dump())
    result = task.get(timeout=100)
    return UserResponse(request=req.request, response=result)
