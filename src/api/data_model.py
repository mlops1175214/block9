from pydantic import BaseModel


class UserRequest(BaseModel):
    request: str
    # max_tokens: int = 0


class UserResponse(BaseModel):
    request: str
    response: list
