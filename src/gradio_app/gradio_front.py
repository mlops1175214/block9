import gradio as gr

# from gradio.pipelines import load_from_pipeline
# from gradio.pipelines_utils import handle_transformers_pipeline
import requests
# from src.api.data_model import UserRequest
# from transformers import pipeline


from pydantic import BaseModel


class UserRequest(BaseModel):
    request: str
    # max_tokens: int = 0


s = requests.Session()


def ask_model_from_front(text):
    req = UserRequest(request=text)
    req.request = text
    resp = s.post(url="http://localhost:8080/ask_model", json=req.model_dump())
    return str(resp.json())


# def choose_model(model):
#     global demo
#     print(model)
#     pipe = pipeline(model=model)
#     fnc_pipe_dict = load_from_pipeline(pipe)
#     del demo
#     demo = gr.Interface(**fnc_pipe_dict)
#     demo.launch()

demo = gr.Interface(
    fn=ask_model_from_front,
    inputs=["text"],
    outputs=["text"],
)

# demo = gr.Interface(
#     fn=choose_model,
#     inputs=["text"],
#     outputs=[],
# )
# pipe = pipeline(model='s-nlp/russian_toxicity_classifier')
# fnc_pipe = load_from_pipeline(pipe)
# print(fnc_pipe)
# demo = gr.Interface.from_pipeline(pipe)
# cometrain/neurotitle-rugpt3-small
# tinkoff-ai/ruDialoGPT-small

# models = []

# with gr.Blocks() as demo:
#     model_selector = gr.Textbox(label='Введи название модели')

#     @gr.render(inputs=model_selector, triggers=[model_selector.submit])
#     def show_split(model_name):
#         pipe = pipeline(model=model_name)
#         # params_from_pipe = load_from_pipeline(pipe)

#         # кусок кода для того, чтобы без функции
#         params_from_pipe = handle_transformers_pipeline(pipe)
#         params_from_pipe['fn'] = None
#         params_from_pipe.pop('preprocess')
#         params_from_pipe.pop('postprocess')


#         if models:
#             last_model = models.pop()
#             del last_model
#         new_model_layout = gr.Interface(**params_from_pipe)
#         models.append(new_model_layout)


demo.launch()
