import os
import celery
from transformers import pipeline

app = celery.Celery(
    "celery_app",
    broker=os.environ["BROKER_URL"],
    backend=os.environ["REDIS_URL"],
)


@app.task
def apply_model_to_text(req):
    classifier = pipeline(
        "text-classification", model="SkolkovoInstitute/russian_toxicity_classifier"
    )
    res = classifier(req["request"])
    return res
