# Block9

## Для сборки контейнеров с celery и приложением
Выполнить из корня проекта

```
docker build . -f .docker/Dockerfile-app -t my-api
```

```
docker build . -f .docker/Dockerfile-celery -t my-celery
```


## Для локальной разработки 

### Если запускать приложения вне докера

из корня проекта:
```
uvicorn src.api.app_service:app --reload
```
```
celery -A src.celery.celery_app worker -P solo --loglevel=info
```


### Если запускать приложения в контейнерах по отдельности

нужно сперва создать отдельную сеть:
```
docker network create mynetwork
```

затем можно поднимать контейнеры:

```
docker run --hostname=my-rabbit --name=my-rabb --network=mynetwork --env=RABBITMQ_DEFAULT_VHOST=my_vhost -p 15672:15672 -p 5672:5672 -d rabbitmq:3-management
```
```
docker run --hostname=my-radis --name=my-radd --network=mynetwork  -p 6379:6379 -d redis:7.4-rc1-bookworm
```
```
docker run --hostname=my-app --network=mynetwork --env=BROKER_URL=amqp://guest:guest@my-rabbit:5672/my_vhost --env=REDIS_URL=redis://my-radis:6379/0 --workdir=/project -p 8080:8080 --restart=no --runtime=runc -d my-api:latest
```
```
docker run --hostname=my-celery --network=mynetwork --env=BROKER_URL=amqp://guest:guest@my-rabbit:5672/my_vhost --env=REDIS_URL=redis://my-radis:6379/0 -d my-celery:latest
```


### Если запускать все контейнеры сразу через docker compose

выполнить из корня проекта
```
docker compose up
```
